// Requires
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var path = require('path');

// Inicializar variables
var app = express();


// CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    next();
});


// Body Parser
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


// Importar rutas
var appRoutes = require('./routes/app');
var usuarioRoutes = require('./routes/usuario');
var inscritoRoutes = require('./routes/inscrito');
var loginRoutes = require('./routes/login');
var cursoRoutes = require('./routes/curso');
var agenteRoutes = require('./routes/agente');
var participanteRoutes = require('./routes/participante');
var busquedaRoutes = require('./routes/busqueda');
var uploadRoutes = require('./routes/upload');
var imagenesRoutes = require('./routes/imagenes');
var nivel1Routes = require('./routes/nivel1');
var nivel2Routes = require('./routes/nivel2');
var nivel3Routes = require('./routes/nivel3');
var nivel4Routes = require('./routes/nivel4');
var ofertaRoutes = require('./routes/oferta');
var frecuenciaRoutes = require('./routes/frecuencia');
var inscritoNotaRoutes = require('./routes/inscritoNota');
var acbRoutes = require('./routes/acb');


// Conexión a la base de datos
mongoose.connection.openUri('mongodb://localhost:27017/controlbiologicoDB', (err, res) => {

    if (err) throw err;

    console.log('Base de datos: \x1b[32m%s\x1b[0m', 'online');

});

// Server index config
// var serveIndex = require('serve-index');
// app.use(express.static(__dirname + '/'))
// app.use('/uploads', serveIndex(__dirname + '/uploads'));



// Rutas
app.use('/usuario', usuarioRoutes);
app.use('/inscrito', inscritoRoutes);
app.use('/curso', cursoRoutes);
app.use('/agente', agenteRoutes);
app.use('/participante', participanteRoutes);
app.use('/login', loginRoutes);
app.use('/busqueda', busquedaRoutes);
app.use('/upload', uploadRoutes);
app.use('/img', imagenesRoutes);
app.use('/nivel1', nivel1Routes);
app.use('/nivel2', nivel2Routes);
app.use('/nivel3', nivel3Routes);
app.use('/nivel4', nivel4Routes);
app.use('/oferta', ofertaRoutes);
app.use('/frecuencia', frecuenciaRoutes);
app.use('/inscritoNota', inscritoNotaRoutes);
app.use('/acb', acbRoutes);

// app.use('/', appRoutes);

// Despliegue
app.use(express.static(path.join(__dirname, 'client')));

// Escuchar peticiones
app.listen(8080, () => {
    // app.listen(80, () => {
    // console.log('Express server puerto 80: \x1b[32m%s\x1b[0m', 'online');
    console.log('Express server puerto 8080: \x1b[32m%s\x1b[0m', 'online');
});