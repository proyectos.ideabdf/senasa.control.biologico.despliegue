var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var nivel2Schema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es necesario'] },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
}, { collection: 'Niveles2' });



module.exports = mongoose.model('Nivel2', nivel2Schema);