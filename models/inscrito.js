var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var estadosValidos = {
    values: ['SOLICITUD', 'ACEPTADO', 'RECHAZADO', 'ANULADO'],
    message: '{VALUE} no es un estado permitido'
};


var inscritoSchema = new Schema({
    estadoInscrito: { type: String, required: false, default: 'SOLICITUD', enum: estadosValidos },
    participante: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    oferta: { type: Schema.Types.ObjectId, ref: 'Oferta' },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
}, { collection: 'inscritos' });



module.exports = mongoose.model('Inscrito', inscritoSchema);