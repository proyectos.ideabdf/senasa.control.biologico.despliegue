var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var nivel3Schema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es necesario'] },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
}, { collection: 'Niveles3' });



module.exports = mongoose.model('Nivel3', nivel3Schema);