var express = require('express');

var mdAutenticacion = require('../middlewares/autenticacion');

var app = express();

var Acb = require('../models/acb');

// ==========================================
// Obtener todos los Acbs
// ==========================================
app.get('/', (req, res, next) => {

    var desde = req.query.desde || 0;
    desde = Number(desde);

    Acb.find({})
        .skip(desde)
        .limit(5)
        .populate('agente')
        .populate('usuario', 'nombre email')
        .exec(
            (err, acbs) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando acbs',
                        errors: err
                    });
                }

                Acb.count({}, (err, conteo) => {
                    res.status(200).json({
                        ok: true,
                        acbs: acbs,
                        total: conteo
                    });

                })

            });
});

// ==========================================
// Obtener acb
// ==========================================
app.get('/:id', (req, res) => {

    var id = req.params.id;

    Acb.findById(id)
        .populate('agente')
        .populate('usuario', 'nombre email img')
        .exec((err, acb) => {

            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error al buscar acb',
                    errors: err
                });
            }

            if (!acb) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'El acb con el id ' + id + ' no existe',
                    errors: { message: 'No existe un acb con ese ID' }
                });
            }

            res.status(200).json({
                ok: true,
                acb: acb
            });

        })


});

// ==========================================
// Actualizar Acb
// ==========================================
app.put('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;
    var body = req.body;

    Acb.findById(id, (err, acb) => {


        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar acb',
                errors: err
            });
        }

        if (!acb) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El acb con el id ' + id + ' no existe',
                errors: { message: 'No existe un acb con ese ID' }
            });
        }

        acb.codigoCepario = body.codigoCepario;
        acb.nombreCientifico = body.nombreCientifico;
        acb.sinonimo1 = body.sinonimo1;
        acb.sinonimo2 = body.sinonimo2;
        acb.sinonimo3 = body.sinonimo3;
        acb.sector = body.sector;
        acb.zona = body.zona;
        acb.temperatura = body.temperatura;
        acb.altitud = body.altitud;
        acb.latitud = body.latitud;
        acb.longitud = body.longitud;
        acb.colector = body.colector;
        acb.fechaColecta = body.fechaColecta;
        acb.ubigeo = body.ubigeo;
        acb.agente = body.agente;
        acb.usuario = req.usuario._id;

        acb.save((err, acbGuardado) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar acb',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                acb: acbGuardado
            });

        });

    });

});



// ==========================================
// Crear un nuevo acb
// ==========================================
app.post('/', mdAutenticacion.verificaToken, (req, res) => {

    var body = req.body;

    var acb = new Acb({
        codigoCepario: body.codigoCepario,
        nombreCientifico: body.nombreCientifico,
        sinonimo1: body.sinonimo1,
        sinonimo2: body.sinonimo2,
        sinonimo3: body.sinonimo3,
        sector: body.sector,
        zona: body.zona,
        temperatura: body.temperatura,
        altitud: body.altitud,
        latitud: body.latitud,
        longitud: body.longitud,
        colector: body.colector,
        fechaColecta: body.fechaColecta,
        ubigeo: body.ubigeo,
        agente: body.agente,
        usuario: req.usuario._id
    });

    acb.save((err, acbGuardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear acb',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            acb: acbGuardado
        });


    });

});


// ============================================
//   Borrar un acb por el id
// ============================================
app.delete('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;

    Acb.findByIdAndRemove(id, (err, acbBorrado) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error borrar acb',
                errors: err
            });
        }

        if (!acbBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un acb con ese id',
                errors: { message: 'No existe un acb con ese id' }
            });
        }

        res.status(200).json({
            ok: true,
            acb: acbBorrado
        });

    });

});


module.exports = app;