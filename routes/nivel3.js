var express = require('express');

var mdAutenticacion = require('../middlewares/autenticacion');

var app = express();

var Nivel3 = require('../models/nivel3');

// ==========================================
// Obtener todos las niveles3
// ==========================================
app.get('/', (req, res, next) => {

    var desde = req.query.desde || 0;
    desde = Number(desde);

    Nivel3.find({})
        .skip(desde)
        .limit(5)
        .populate('usuario', 'nombre email')
        .exec(
            (err, niveles3) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando nivel3',
                        errors: err
                    });
                }

                Nivel3.count({}, (err, conteo) => {

                    res.status(200).json({
                        ok: true,
                        niveles3: niveles3,
                        total: conteo
                    });
                })

            });
});

// ==========================================
//  Obtener Nivel3 por ID
// ==========================================
app.get('/:id', (req, res) => {

    var id = req.params.id;

    Nivel3.findById(id)
        .populate('usuario', 'nombre img email')
        .exec((err, nivel3) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error al buscar nivel3',
                    errors: err
                });
            }

            if (!nivel3) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'El nivel3 con el id ' + id + 'no existe',
                    errors: { message: 'No existe un nivel3 con ese ID' }
                });
            }
            res.status(200).json({
                ok: true,
                nivel3: nivel3
            });
        })
})





// ==========================================
// Actualizar Nivel3
// ==========================================
app.put('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;
    var body = req.body;

    Nivel3.findById(id, (err, nivel3) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar nivel3',
                errors: err
            });
        }

        if (!nivel3) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El nivel3 con el id ' + id + ' no existe',
                errors: { message: 'No existe un nivel3 con ese ID' }
            });
        }


        nivel3.nombre = body.nombre;
        nivel3.usuario = req.usuario._id;

        nivel3.save((err, nivel3Guardado) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar nivel3',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                nivel3: nivel3Guardado
            });

        });

    });

});



// ==========================================
// Crear un nuevo nivel3
// ==========================================
app.post('/', mdAutenticacion.verificaToken, (req, res) => {

    var body = req.body;

    var nivel3 = new Nivel3({
        nombre: body.nombre,
        usuario: req.usuario._id
    });

    nivel3.save((err, nivel3Guardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear nivel3',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            nivel3: nivel3Guardado
        });


    });

});


// ============================================
//   Borrar un nivel3 por el id
// ============================================
app.delete('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;

    Nivel3.findByIdAndRemove(id, (err, nivel3Borrado) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error borrar nivel3',
                errors: err
            });
        }

        if (!nivel3Borrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un nivel3 con ese id',
                errors: { message: 'No existe un nivel3 con ese id' }
            });
        }

        res.status(200).json({
            ok: true,
            nivel3: nivel3Borrado
        });

    });

});


module.exports = app;