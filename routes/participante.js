var express = require('express');

var mdAutenticacion = require('../middlewares/autenticacion');

var app = express();

var Participante = require('../models/participante');

// ==========================================
// Obtener todos los participantes
// ==========================================
app.get('/', (req, res, next) => {

    var desde = req.query.desde || 0;
    desde = Number(desde);

    Participante.find({})
        .skip(desde)
        .limit(5)
        .populate('usuario', 'nombre email')
        .exec(
            (err, participantes) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando participantes',
                        errors: err
                    });
                }

                Participante.count({}, (err, conteo) => {
                    res.status(200).json({
                        ok: true,
                        participantes: participantes,
                        total: conteo
                    });

                })

            });
});

// ==========================================
// Obtener participante
// ==========================================
app.get('/:id', (req, res) => {

    var id = req.params.id;

    Participante.findById(id)
        .populate('usuario', 'nombre email img')
        .exec((err, participante) => {

            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error al buscar participante',
                    errors: err
                });
            }

            if (!participante) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'El participante con el id ' + id + ' no existe',
                    errors: { message: 'No existe un participante con ese ID' }
                });
            }
            res.status(200).json({
                ok: true,
                participante: participante
            });
        });
});

// ==========================================
// Obtener participante x Usuario
// ==========================================
app.get('/usuario/:idUsuario', (req, res) => {

    var idUsuario = req.params.idUsuario;

    Participante.find({ idUsuario })
        .populate('usuario', 'nombre email img')
        .exec((err, participante) => {

            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error al buscar participante',
                    errors: err
                });
            }

            if (!participante) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'El participante con el id ' + id + ' no existe',
                    errors: { message: 'No existe un participante con ese ID' }
                });
            }
            res.status(200).json({
                ok: true,
                participante: participante
            });
        });
});

// ==========================================
// Actualizar Participante
// ==========================================
app.put('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;
    var body = req.body;

    Participante.findById(id, (err, participante) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar participante',
                errors: err
            });
        }

        if (!participante) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El participante con el id ' + id + ' no existe',
                errors: { message: 'No existe un participante con ese ID' }
            });
        }
        participante.nombres = body.nombres;
        participante.apellidoPaterno = body.apellidoPaterno;
        participante.apellidoMaterno = body.apellidoMaterno;
        participante.dni = body.dni;
        participante.sexo = body.sexo;
        participante.movil = body.movil;
        participante.fijo = body.fijo;
        participante.direccion = body.direccion;
        participante.distrito = body.distrito;
        participante.provincia = body.provincia;
        participante.region = body.region;
        participante.fechaNacimiento = body.fechaNacimiento;
        participante.distritoNacimiento = body.distritoNacimiento;
        participante.carreraProfesional = body.carreraProfesional;
        participante.anioTerminoPreGrado = body.anioTerminoPreGrado;
        participante.situacionAcademicaActual = body.situacionAcademicaActual;
        participante.estudiosPostGrado = body.estudiosPostGrado;
        participante.situacionLaboral = body.situacionLaboral;
        participante.empleado = body.empleado;
        participante.cargo = body.cargo;
        participante.nombreEmpresa = body.nombreEmpresa;
        // participante.usuario = req.usuario._id;
        participante.save((err, participanteGuardado) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar participante',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                participante: participanteGuardado
            });
        });
    });
});

// ==========================================
// Crear un nuevo participante
// ==========================================
app.post('/', (req, res) => {

    var body = req.body;

    var participante = new Participante({
        nombres: body.nombres,
        apellidoPaterno: body.apellidoPaterno,
        apellidoMaterno: body.apellidoMaterno,
        dni: body.dni,
        sexo: body.sexo,
        movil: body.movil,
        fijo: body.fijo,
        direccion: body.direccion,
        distrito: body.distrito,
        provincia: body.provincia,
        region: body.region,
        fechaNacimiento: body.fechaNacimiento,
        distritoNacimiento: body.distritoNacimiento,
        carreraProfesional: body.carreraProfesional,
        anioTerminoPreGrado: body.anioTerminoPreGrado,
        situacionAcademicaActual: body.situacionAcademicaActual,
        estudiosPostGrado: body.estudiosPostGrado,
        situacionLaboral: body.situacionLaboral,
        empleado: body.empleado,
        cargo: body.cargo,
        nombreEmpresa: body.nombreEmpresa,
        usuario: body.usuario
    });

    participante.save((err, participanteGuardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear participante',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            participante: participanteGuardado
        });
    });
});


// ============================================
//   Borrar un participante por el id
// ============================================
app.delete('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;

    Participante.findByIdAndRemove(id, (err, participanteBorrado) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error borrar participante',
                errors: err
            });
        }

        if (!participanteBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un participante con ese id',
                errors: { message: 'No existe un participante con ese id' }
            });
        }

        res.status(200).json({
            ok: true,
            participante: participanteBorrado
        });

    });

});


module.exports = app;